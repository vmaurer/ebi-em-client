# ebi-em-client

## Rational
This software provides an interface for many different [ebi](https://www.ebi.ac.uk/) APIs dealing with protein structures and EM-maps.

## Installation
This software can be installed from source as follows:
```
git clone https://git.embl.de/vmaurer/ebi-em-client
cd ebi-em-client
python3 -mpip install .
```

## Documentation
As of right now, the majority of functions are lacking documentation or are deprecated. Users should thus use the command line interface available after installation as follows:

To download a structure file in [mmcif](https://mmcif.wwpdb.org/) format:
```
ebiq_download.py -i 6hqe
```

To download an em-map in [mrc/ccp4](https://www.ccpem.ac.uk/mrc_format/mrc2014.php) format:
```
ebiq_download.py -i -m EMD-0252
```
and to include associated structure drop the -m tag:
```
ebiq_download.py -i EMD-0252
```
The optional -o argument controls the output directory. If -o is not provided, the output directory is the current directory.

For the sake of reproducibility, it is advantageous to also compute checksums. This can be achieved using the -c option. For em-maps with associated structures the following computes the checksum of all checksums in the created folder:
 ```
ebiq_download.py -i EMD-0252 -o /path/to/destination -c
```
