from math import nan
from collections.abc import Callable
import asyncio

import httpx
from ssl import SSLZeroReturnError
from bs4 import BeautifulSoup

def flatten_list(lst: list):
    # https://stackoverflow.com/questions/12472338/flattening-a-list-recursively chyanog
    return sum(map(flatten_list, lst), []) if isinstance(lst, list) else [lst]


def deep_get(dct, keys, exclude=[]):
    """
    Extracts fields from a json like structure

    Input:

                    dct: json response

                    keys: keys in order leading to desired element

                    exclude:    keys in the dictionary that will be ignored as
                                            potential paths

    Return:

                    generator of matches

    """

    if isinstance(keys, list):
        keys = keys.copy()
    else:
        keys = [keys]

    if isinstance(dct, list):
        for subdict in dct:
            for res in deep_get(subdict, keys, exclude):
                yield res

    if isinstance(dct, dict):
        for k, v in dct.items():
            keys_copy = keys.copy()
            if k in exclude:
                continue

            if k == keys_copy[0] and len(keys_copy) == 1:
                yield v
            if isinstance(v, dict):
                if k == keys_copy[0] and len(keys_copy) > 1:
                    keys_copy.pop(0)
                yield from deep_get(v, keys_copy, exclude)
            elif isinstance(v, list):
                if k == keys_copy[0]:
                    keys_copy.pop(0)
                for d in v:
                    yield from deep_get(d, keys_copy, exclude)


def isnested(lst):
    """Check if a given list is nested"""
    try:
        # length has to be two
        if isinstance(lst[1], list):
            return True
    except Exception:
        return False
    else:
        return False


def default_parser(response: dict, request_structure_func: Callable) -> dict:
    names = list(response.keys())
    if request_structure_func == _get_emdbid_info_request_structure_newApi:
        names = [response["emdb_id"]]

    result = []
    for name in names:
        request_structure = request_structure_func(name)
        data = {"name": name}
        for k, v in request_structure.items():
            if isnested(v):
                response_value = list(deep_get(response, keys=v[0], exclude=v[1]))
            else:
                response_value = list(deep_get(response, v))
            if len(response_value) == 0:
                response_value = [nan]
            if len(response_value) == 1:
                response_value = response_value[0]
            if type(response_value) == dict:
                response_value = list(set([x for x in response_value.keys()]))
                if len(response_value) == 1:
                    response_value = response_value[0]
            data[k] = response_value
        result.append(data)
    return result


def async_run(func):
    def inner_func(*args, **kwargs):
        return asyncio.run(func(*args, **kwargs))

    return inner_func


@async_run
async def async_request_handler(
    urls: [str], request_structure: Callable, parser: Callable = default_parser
) -> ([dict], [str], [str]):
    headers = {
        "user-agent": "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7)"
        " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36"
    }
    async with httpx.AsyncClient() as client:
        tasks = (
            client.get(
                url=url,
                headers=headers,
                timeout=None,
            )
            for url in urls
        )
        results = await asyncio.gather(*tasks, return_exceptions=True)
    filtered, failed, retry = [], [], []
    for url, result in zip(urls, results):
        if result.__class__ in (
            httpx.ConnectError,
            httpx.RemoteProtocolError,
            SSLZeroReturnError,
        ):
            retry.append(url)
            print(f"Connect error for: {url}. Skipping ...")
            continue
        if result.status_code != 200:
            print(f"Status code {result.status_code} for: {url}. Skipping ...")
            failed.append(url)
            continue
        filtered.append(result)
    results = [parser(result.json(), request_structure) for result in filtered]
    results = flatten_list(results)
    return results, failed, retry


def emdbid_info(emdbids: [str]) -> [dict]:
    if emdbids.__class__ == str:
        emdbids = [emdbids]
    urls = [
        f"https://www.ebi.ac.uk/pdbe/api/emdb/entry/all/{emdbid}" for emdbid in emdbids
    ]
    return async_request_handler(urls, _get_emdbid_info_request_structure)


def _get_emdbid_info_request_structure(emdbid: str) -> dict:
    request_structure = {
        # deposition metadata
        "deposition_date": [emdbid, "deposition", "deposition_date"],
        "fitted_structures": [emdbid, "deposition", "fitted_pdb_id_list", "pdb_id"],
        "publication_title": [emdbid, "deposition", "primary_publication", "title"],
        "publication_journal": [emdbid, "deposition", "primary_publication", "journal"],
        # experimental metadata regarding imaging
        "number_images": [emdbid, "experiment", "image_acquisition", "number_images"],
        "acceleration_voltage": [emdbid, "imaging", "acceleration_voltage", "value"],
        "acceleration_voltage_unit": [
            emdbid,
            "imaging",
            "acceleration_voltage",
            "units",
        ],
        "detector": [emdbid, "experiment", "imaging", "detector"],
        "electron_dose": [emdbid, "experiment", "imaging", "electron_dose", "value"],
        "electron_dose_unit": [
            emdbid,
            "experiment",
            "imaging",
            "electron_dose",
            "units",
        ],
        "electron_source": [emdbid, "experiment", "imaging", "electron_source"],
        "illumination_mode": [emdbid, "experiment", "imaging", "illumination_mode"],
        "imaging_mode": [emdbid, "experiment", "imaging", "imaging_mode"],
        "microscope": [emdbid, "experiment", "imaging", "microscope"],
        "nominal_minimum_defocus": [
            emdbid,
            "experiment",
            "imaging",
            "nominal_minimum_defocus",
            "value",
        ],
        "nominal_minimum_defocus_unit": [
            emdbid,
            "experiment",
            "imaging",
            "nominal_minimum_defocus",
            "units",
        ],
        "spherical_aberration_coefficient": [
            emdbid,
            "experiment",
            "imaging",
            "spherical_aberration_coefficient",
            "value",
        ],
        "spherical_aberration_coefficient_unit": [
            emdbid,
            "experiment",
            "imaging",
            "spherical_aberration_coefficient",
            "units",
        ],
        # experimental metadata regarding specimen
        "aggregation_state": [
            emdbid,
            "experiment",
            "specimen_preparation",
            "aggregation_state",
        ],
        "buffer_ph": [emdbid, "experiment", "specimen_preparation", "buffer", "ph"],
        "specimen_concentration": [
            emdbid,
            "experiment",
            "specimen_preparation",
            "specimen_concentration",
            "value",
        ],
        "specimen_concentration_unit": [
            emdbid,
            "experiment",
            "specimen_preparation",
            "specimen_concentration",
            "units",
        ],
        # experimental metadaca regarding vitrification
        "cryogen": [emdbid, "experiment", "vitrification", "cryogen"],
        "vitrification_instrument": [
            emdbid,
            "experiment",
            "vitrification",
            "instrument",
        ],
        # map metadata
        "map_columns": [emdbid, "map", "spacing", "column"],
        "map_rows": [emdbid, "map", "spacing", "row"],
        "map_sections": [emdbid, "map", "spacing", "section"],
        "map_average": [emdbid, "map", "statistics", "average"],
        "map_minimum": [emdbid, "map", "statistics", "minimum"],
        "map_maximum": [emdbid, "map", "statistics", "maximum"],
        "map_standard_deviation": [emdbid, "map", "statistics", "standard_deviation"],
        "map_contour_level": [emdbid, "map", "contour_level", "value"],
        "map_contour_level_source": [emdbid, "map", "contour_level", "source"],
        "cell_a_degrees": [emdbid, "map", "cell", "alpha", "value"],
        "cell_a_degrees_units": [emdbid, "map", "cell", "alpha", "units"],
        "cell_b_degrees": [emdbid, "map", "cell", "beta", "value"],
        "cell_b_degrees_units": [emdbid, "map", "cell", "beta", "units"],
        "cell_c_degrees": [emdbid, "map", "cell", "gamma", "value"],
        "cell_c_degrees_units": [emdbid, "map", "cell", "gamma", "units"],
        # apix
        "pixel_spacing_x": [emdbid, "map", "pixel_spacing", "x", "value"],
        "pixel_spacing_x_unit": [emdbid, "map", "pixel_spacing", "x", "units"],
        "pixel_spacing_y": [emdbid, "map", "pixel_spacing", "y", "value"],
        "pixel_spacing_y_unit": [emdbid, "map", "pixel_spacing", "y", "units"],
        "pixel_spacing_z": [emdbid, "map", "pixel_spacing", "z", "value"],
        "pixel_spacing_z_unit": [emdbid, "map", "pixel_spacing", "z", "units"],
        # processing metadata
        "method": [emdbid, "processing", "method"],
        "reconstruction_algorithm": [
            emdbid,
            "processing",
            "reconstruction",
            "algorithm",
        ],
        "resolution": [emdbid, "processing", "reconstruction", "resolution_by_author"],
        "resolution_method": [
            emdbid,
            "processing",
            "reconstruction",
            "resolution_method",
        ],
        "software": [emdbid, "processing", "reconstruction", "software"],
        "processing_details": [emdbid, "processing", "details"],
        # sample metadata
        "sample_components": [emdbid, "sample", "number_components"],
        "sample_organism_source": [
            emdbid,
            "sample",
            "scientific_organism_name",
            "ncbi_tax_id",
        ],
        "sample_category": [emdbid, "sample", "sample_category"],
        "theoretical_molecular_mass": [
            emdbid,
            "sample",
            "theoretical_molecular_mass",
            "value",
        ],
        "theoretical_molecular_mass_unit": [
            emdbid,
            "sample",
            "theoretical_molecular_mass",
            "units",
        ],
    }
    return request_structure


def emdbid_info_newApi(emdbids: [str]) -> [dict]:
    if emdbids.__class__ == str:
        emdbids = [emdbids]
    urls = [f"https://www.ebi.ac.uk/emdb/api/entry/{emdbid}" for emdbid in emdbids]
    return async_request_handler(urls, _get_emdbid_info_request_structure_newApi)


def _get_emdbid_info_request_structure_newApi(emdbid: str) -> dict:
    request_structure = {
        # deposition metadata
        "deposition_date": ["admin", "key_dates", "deposition"],
        "fitted_structures": ["crossreferences", "pdb_list", "pdb_id"],
        "publication_title": ["crossreferences", "primary_citation", "title"],
        "publication_journal": ["crossreferences", "primary_citation", "journal"],
        # structure_determination_list metadata
        "aggregation_state": ["structure_determination", "aggregation_state"],
        # "image_processing_software_ctf" : [emdbid, "structure_determination",
        #     "image_processing", "ctf_correction", "software"],
        # "image_processing_version_ctf" : [emdbid, "structure_determination",
        #     "image_processing", "ctf_correction", "version"],
        "image_processing_details": [
            "structure_determination",
            "image_processing",
            "details",
        ],
        "image_processing_angle_assignment": [
            "structure_determination",
            "final_angle_assignment",
            "type_",
        ],
        "number_images_used": [
            "structure_determination",
            "final_reconstruction",
            "number_images_used",
        ],
        "resolution": ["structure_determination", "resolution", "valueOf_"],
        "resolution_unit": ["structure_determination", "resolution", "units"],
        "resolution_source": ["structure_determination", "resolution", "res_type"],
        "resolution_method": ["structure_determination", "resolution_method"],
        "reconstruction_software": [
            "structure_determination",
            "final_reconstruction",
            "software",
            "name",
        ],
        "reconstruction_software_version": [
            "structure_determination",
            "final_reconstruction",
            "software",
            "version",
        ],
        "reconstruction_instance": ["structure_determination", "instance_type"],
        "particle_selection": [
            "structure_determination",
            "particle_selection",
            "instance_type",
        ],
        "particle_selected": [
            "structure_determination",
            "particle_selection",
            "number_selected",
        ],
        "reconstruction_method": ["structure_determination", "method"],
        "acceleration_voltage": [
            "structure_determination",
            "microscopy",
            "acceleration_voltage",
            "valueOf_",
        ],
        "acceleration_voltage_unit": [
            "structure_determination",
            "microscopy",
            "acceleration_voltage",
            "units",
        ],
        "c2_aperture_diameter": [
            "structure_determination",
            "microscopy",
            "c2_aperture_diameter",
            "valueOf_",
        ],
        "c2_aperture_diameter_unit": [
            "structure_determination",
            "microscopy",
            "c2_aperture_diameter",
            "units",
        ],
        "cooling_holder_cryogen": [
            "structure_determination",
            "microscopy",
            "cooling_holder_cryogen",
        ],
        "electron_source": ["structure_determination", "microscopy", "electron_source"],
        "illumination_mode": [
            "structure_determination",
            "microscopy",
            "illumination_mode",
        ],
        "average_electron_dose_per_image": [
            "structure_determination",
            "microscopy",
            "image_recording",
            "average_electron_dose_per_image",
            "valueOf_",
        ],
        "average_electron_dose_per_image_unit": [
            "structure_determination",
            "microscopy",
            "image_recording",
            "average_electron_dose_per_image",
            "units",
        ],
        "average_exposure_time": [
            "structure_determination",
            "microscopy",
            "image_recording",
            "average_exposure_time",
            "valueOf_",
        ],
        "average_exposure_time_units": [
            "structure_determination",
            "microscopy",
            "image_recording",
            "average_exposure_time",
            "units",
        ],
        "recording_details": [
            "structure_determination",
            "microscopy",
            "image_recording",
            "details",
        ],
        "number_grids_imaged": [
            "structure_determination",
            "microscopy",
            "image_recording",
            "number_grids_imaged",
        ],
        "number_real_images": [
            "structure_determination",
            "microscopy",
            "image_recording",
            "number_real_images",
        ],
        "imaging_mode": ["structure_determination", "microscopy", "imaging_mode"],
        "instance_type": ["structure_determination", "microscopy", "instance_type"],
        "microscope": ["structure_determination", "microscopy", "microscope"],
        "nominal_cs": [
            "structure_determination",
            "microscopy",
            "nominal_cs",
            "valueOf_",
        ],
        "nominal_cs_unit": [
            "structure_determination",
            "microscopy",
            "nominal_cs",
            "units",
        ],
        "nominal_defocus_min": [
            "structure_determination",
            "microscopy",
            "nominal_defocus_min",
            "valueOf_",
        ],
        "nominal_defocus_min_unit": [
            "structure_determination",
            "microscopy",
            "nominal_defocus_min",
            "valueOf_",
        ],
        "buffer_ph": [
            "structure_determination",
            "specimen_preparation_list",
            "buffer",
            "ph",
        ],
        "grid_model": [
            "structure_determination",
            "specimen_preparation_list",
            "grid",
            "model",
        ],
        "cryogen": [
            "structure_determination",
            "specimen_preparation_list",
            "vitrification",
            "cryogen_name",
        ],
        "vitrification_instrument": [
            "structure_determination",
            "specimen_preparation_list",
            "vitrification",
            "instrument",
        ],
        # sample metadata
        "sample_name": ["sample", "name", "valueOf_"],
        "sample_weight": ["sample", "supramolecule", "molecular_weight", "valueOf_"],
        "sample_weight_unit": ["sample", "supramolecule", "molecular_weight", "units"],
        "sample_organism_taxid": ["sample", "organism", "ncbi"],
        # map metadata
        "map_columns": ["map", "dimensions", "col"],
        "map_rows": ["map", "dimensions", "row"],
        "map_sections": ["map", "dimensions", "sec"],
        "map_average": ["map", "statistics", "average"],
        "map_minimum": ["map", "statistics", "minimum"],
        "map_maximum": ["map", "statistics", "maximum"],
        # apix
        "pixel_spacing_x": ["map", "pixel_spacing", "x", "valueOf_"],
        "pixel_spacing_x_unit": ["map", "pixel_spacing", "x", "units"],
        "pixel_spacing_y": ["map", "pixel_spacing", "y", "valueOf_"],
        "pixel_spacing_y_unit": ["map", "pixel_spacing", "y", "units"],
        "pixel_spacing_z": ["map", "pixel_spacing", "z", "valueOf_"],
        "pixel_spacing_z_unit": ["map", "pixel_spacing", "z", "units"],
        "map_standard_deviation": ["map", "statistics", "std"],
        "map_contour_level": ["map", "contour", "level"],
        "map_contour_level_source": ["map", "contour", "source"],
        "cell_a_degrees": ["map", "cell", "alpha", "valueOf_"],
        "cell_a_degrees_units": ["map", "cell", "alpha", "units"],
        "cell_b_degrees": ["map", "cell", "beta", "valueOf_"],
        "cell_b_degrees_units": ["map", "cell", "beta", "units"],
        "cell_c_degrees": ["map", "cell", "gamma", "valueOf_"],
        "cell_c_degrees_units": ["map", "cell", "gamma", "units"],
    }
    return request_structure


def pdbid_info(pdbids: [str]) -> [dict]:
    if pdbids.__class__ == str:
        pdbids = [pdbids]
    urls = [
        f"https://www.ebi.ac.uk/pdbe/api/pdb/entry/experiment/{pdbid}"
        for pdbid in pdbids
    ]
    return async_request_handler(urls, _get_pdbid_info_request_structure)


def _get_pdbid_info_request_structure(name: str) -> dict:
    request_structure = {
        "experimental_method": [name, "experimental_method"],
        # fitting metadata
        "initial_model_list": [name, "fitting", "initial_model_list"],
        "overall_b_value": [name, "fitting", "overall_b_value"],
        "refinement_protocol": [name, "fitting", "refinement_protocol"],
        "refinement_space": [name, "fitting", "refinement_space"],
        "software": [name, "fitting", "software"],
        "target_criteria": [name, "fitting", "target_criteria"],
        # image acquisition
        "bits_per_pixel": [name, "image_acquisition", "bits_per_pixel"],
        "acquisition_details": [name, "image_acquisition", "details"],
        "number_images": [name, "image_acquisition", "number_images"],
        "sampling_interval": [name, "image_acquisition", "sampling_interval"],
        "scanner": [name, "image_acquisition", "scanner"],
        # imaging metadata
        "acceleration_voltage": [name, "imaging", "acceleration_voltage"],
        "astigmatism": [name, "imaging", "astigmatism"],
        "astigmatism_correction_details": [
            name,
            "imaging",
            "astigmatism_correction_details",
        ],
        "calibrated_magnification": [name, "imaging", "calibrated_magnification"],
        "date": [name, "imaging", "date"],
        "imaging_details": [name, "imaging", "details"],
        "detector": [name, "imaging", "detector"],
        "detector_details": [name, "imaging", "detector_details"],
        "detector_distance": [name, "imaging", "detector_distance"],
        "electron_dose": [name, "imaging", "electron_dose"],
        "em_imaging_mode": [name, "imaging", "em_imaging_mode"],
        "energy_filter": [name, "imaging", "energy_filter"],
        "energy_window": [name, "imaging", "energy_window"],
        "holder_details": [name, "imaging", "holder_details"],
        "holder_model": [name, "imaging", "holder_model"],
        "illumination_mode": [name, "imaging", "illumination_mode"],
        "maximum_temperature": [name, "imaging", "maximum_temperature"],
        "microscope": [name, "imaging", "microscope"],
        "minimum_temperature": [name, "imaging", "minimum_temperature"],
        "nominal_magnification": [name, "imaging", "nominal_magnification"],
        "nominal_maximum_defocus": [name, "imaging", "nominal_maximum_defocus"],
        "nominal_minimum_defocus": [name, "imaging", "nominal_minimum_defocus"],
        "spherical_aberration_coef": [name, "imaging", "spherical_aberration_coef"],
        "temperature": [name, "imaging", "temperature"],
        "tilt_maximum_angle": [name, "imaging", "tilt_maximum_angle"],
        "tilt_minimum_angle": [name, "imaging", "tilt_minimum_angle"],
        # resolution and processing metadata
        "resolution": [name, "resolution"],
        "processing": [name, "processing"],
        # specimen_preparation metadata
        "buffer_name": [name, "specimen_preparation", "buffer", "name"],
        "buffer_details": [name, "specimen_preparation", "buffer", "details"],
        "buffer_ph": [name, "specimen_preparation", "buffer", "ph"],
        "crystal_grow_details": [name, "specimen_preparation", "crystal_grow_details"],
        "specimen_concentration": [
            name,
            "specimen_preparation",
            "specimen_concentration",
        ],
        "cryogen": [name, "specimen_preparation", "vitrification", "cryogen"],
        "vitrification_details": [
            name,
            "specimen_preparation",
            "vitrification",
            "details",
        ],
        "vitrification_instrument": [
            name,
            "specimen_preparation",
            "vitrification",
            "instrument",
        ],
    }
    return request_structure


def emdbid_fitted_structures(emdbids: [str]) -> [dict]:
    if emdbids.__class__ == str:
        emdbids = [emdbids]
    urls = [
        f"https://www.ebi.ac.uk/pdbe/api/emdb/entry/fitted/{emdbid}"
        for emdbid in emdbids
    ]
    return async_request_handler(urls, _get_emdbid_fitted_structures_request_structure)


def _get_emdbid_fitted_structures_request_structure(name: str) -> dict:
    request_structure = {"pdb_id": [name, "fitted_emdb_id_list", "pdb_id"]}
    return request_structure


def pdb_to_uniprot(pdbids: [str]) -> dict:
    if pdbids.__class__ == str:
        pdbids = [pdbids]
    urls = [
        f"https://www.ebi.ac.uk/pdbe/api/mappings/uniprot/{pdbid}" for pdbid in pdbids
    ]
    return async_request_handler(
        urls, _get_pdb_to_uniprot_request_structure, uniprot_parser
    )


def _get_pdb_to_uniprot_request_structure(pdb_id):
    return {
        "UniProt": [pdb_id, "UniProt"],
        "Identifier": [pdb_id, "UniProt", "identifier"],
        "Chain": [pdb_id, "UniProt", "mappings", "chain_id"],
        "Entity": [pdb_id, "UniProt", "mappings", "entity_id"],
        "PDBStart": [pdb_id, "UniProt", "mappings", "start", "author_residue_number"],
        "PDBStop": [pdb_id, "UniProt", "mappings", "end", "author_residue_number"],
        "MMCIFStart": [pdb_id, "UniProt", "mappings", "start", "residue_number"],
        "MMCIFStop": [pdb_id, "UniProt", "mappings", "end", "residue_number"],
        "UniprotStart": [pdb_id, "UniProt", "mappings", "unp_start"],
        "UniprotStop": [pdb_id, "UniProt", "mappings", "unp_end"],
    }


def uniprot_parser(response: dict, request_structure_func: Callable) -> dict:
    names = list(response.keys())

    result = []
    for name in names:
        request_structure = request_structure_func(name)
        data = {
            "name": name,
            "UniProt": [],
            "Identifier": [],
            "Entity": [],
            "Chain": [],
            "PDBStartAuthor": [],
            "PDBStopAuthor": [],
            "PDBStart": [],
            "PDBStop": [],
            "UniprotStart": [],
            "UniprotStop": [],
        }
        response = response[name]["UniProt"]
        for uniprot in response:
            ident = [response[uniprot]["identifier"]]

            chain = list(deep_get(response[uniprot], ["mappings", "chain_id"]))
            entity = list(deep_get(response[uniprot], ["mappings", "entity_id"]))
            pdb_start = list(
                deep_get(
                    response[uniprot], ["mappings", "start", "author_residue_number"]
                )
            )
            pdb_stop = list(
                deep_get(
                    response[uniprot], ["mappings", "end", "author_residue_number"]
                )
            )

            mmcif_start = list(
                deep_get(response[uniprot], ["mappings", "start", "residue_number"])
            )
            mmcif_stop = list(
                deep_get(response[uniprot], ["mappings", "end", "residue_number"])
            )

            unp_start = list(deep_get(response[uniprot], ["mappings", "unp_start"]))
            unp_stop = list(deep_get(response[uniprot], ["mappings", "unp_end"]))

            assert len(chain) == len(entity) == len(unp_start) == len(unp_stop)

            n_entity = len(entity)
            data["UniProt"].extend([uniprot] * n_entity)
            data["Identifier"].extend(ident * n_entity)
            data["Entity"].extend(entity)
            data["Chain"].extend(chain)
            data["PDBStartAuthor"].extend(pdb_start)
            data["PDBStopAuthor"].extend(pdb_stop)
            data["PDBStart"].extend(mmcif_start)
            data["PDBStop"].extend(mmcif_stop)
            data["UniprotStart"].extend(unp_start)
            data["UniprotStop"].extend(unp_stop)

        result.append(data)
    return result


def list_available_emdbids():
    emdbids, failed, retry = [], [], []
    response = httpx.get("http://ftp.ebi.ac.uk/pub/databases/emdb/structures/")
    if response.status_code != 200:
        print(f"Status code {result.status_code} for: {url}. Skipping ...")
        return emdbids, [url], retry
    soup = BeautifulSoup(response.text, features="html.parser")
    emdbids = [element.text for element in soup.find_all("a", href=True)]
    emdbids = [eid.replace("/", "") for eid in emdbids if eid.startswith("EMD")]

    return emdbids, failed, retry
