import os
import ftputil
import datetime
import httpx

from shutil import rmtree, copyfileobj
from sys import exit
from time import sleep
from random import randrange
from os.path import join, exists, abspath, basename

import wget
import gzip

from .metadata_queries import emdbid_info_newApi, emdbid_info, async_run

FTP_USER = "anonymous"
FTP_PASSWORD = "valentin.maurer@embl-hamburg.de"
MAX_RETRY = 3


class Time:
    """A class to calculate the time elapsed during code execution."""

    def __init__(self):
        self.start = datetime.datetime.now()

    def elapsed_display(self):
        time_elapsed = str(self.elapsed()).split(".")[0]
        print(f"Time elapsed: {time_elapsed} [%H:%M:%S]")

    def elapsed(self):
        self.end = datetime.datetime.now()
        time_elapsed = self.end - self.start
        return time_elapsed


def validate_download(*args) -> bool:
    """Returns true if all the files in args exist, otherwise False."""
    for filepath in args:
        if not exists(filepath):
            return False
    return True


def _validate_download(func):
    """Decorator to retry downloading a file if it fails."""
    retry_counter = 0

    def inner(*args, **kwargs):
        target_path, download_ok = func(*args, **kwargs)
        nonlocal retry_counter
        if not download_ok and retry_counter <= MAX_RETRY:
            retry_counter += 1
            sleep_time = randrange(10, 30)
            print(
                "Bundle download failed. "
                + f"Retry {retry_counter} in {sleep_time} seconds."
            )
            rmtree(target_path)
            sleep(sleep_time)
            return inner(*args, **kwargs)
        if retry_counter > MAX_RETRY:
            print(f"Bundle download failed {retry_counter} times. Exiting")
            exit(1)
        return target_path

    return inner


@_validate_download
def download_map(emdb_id: str, dest_dir: str):
    """Downloads a map file for a given EMDB ID from the EBI FTP server."""
    url = (
        "ftp://ftp.ebi.ac.uk/pub/databases/emdb/structures/"
        + f"{emdb_id}/map/{emdb_id.replace('-', '_').lower()}.map.gz"
    )

    target_path = os.path.join(dest_dir, f"{emdb_id.replace('-', '_').lower()}.map.gz")
    if os.path.exists(target_path):
        os.remove(target_path)
    target_path = wget.download(url, dest_dir, bar=None)

    download_ok = validate_download(target_path)

    return target_path, download_ok


@_validate_download
def download_pdb(pdb_id: str, dest_dir: str, filetype: str = "cif"):
    """Downloads a PDB file for a given PDB ID from the RCSB PDB server."""
    url = f"http://files.rcsb.org/download/{pdb_id}.{filetype}.gz"
    target_path = os.path.join(dest_dir, f"{pdb_id}.{filetype}.gz")

    target_path = wget.download(url, target_path, bar=None)
    target_path = _extract_gzip(target_path)

    download_ok = validate_download(target_path)

    return target_path, download_ok


@async_run
async def download_pdbs(pdb_ids: [str], dest_dir: str, filetype: str = "cif"):
    """Downloads multiple PDB files for a given list of PDB IDs asynchronously from the
    RCSB PDB server."""
    urls = [
        f"http://files.rcsb.org/download/{pdb_id}.{filetype}.gz" for pdb_id in pdb_ids
    ]
    target_paths = [
        os.path.join(dest_dir, f"{pdb_id}.{filetype}.gz") for pdb_id in pdb_ids
    ]

    async with httpx.AsyncClient() as client:
        for url, target in zip(urls, target_paths):
            with open(target, "wb") as ofile:
                async with client.stream("GET", url) as response:
                    async for chunk in response.aiter_bytes():
                        ofile.write(chunk)

    target_paths = [_extract_gzip(target) for target in target_paths]
    download_ok = all([validate_download(target) for target in target_paths])

    return target_paths, download_ok


@_validate_download
def download_bundle(emdb_id: str, dest_dir: str):
    """Downloads a bundle of files for a given EMDB ID from the EBI FTP server,
    including a map file and associated structures."""
    emdb_id = emdb_id.upper()

    target_path = os.path.join(dest_dir, emdb_id.upper())
    ftp_path = f"pub/databases/emdb/structures/{emdb_id.upper()}"

    with ftputil.FTPHost("ftp.ebi.ac.uk", FTP_USER, FTP_PASSWORD) as host:
        for directory, subdirectory, files in host.walk(ftp_path):
            if len(files) == 0:
                continue
            path_on_os = directory.replace(ftp_path, target_path)
            os.makedirs(path_on_os, exist_ok=True)
            for file in files:
                path_on_ftp = host.path.join(directory, file)
                host.download(path_on_ftp, os.path.join(path_on_os, file))

    # Download associated structures
    structure_dir = os.path.join(target_path, "fittedModels/mmCIF")
    os.makedirs(structure_dir, exist_ok=True)
    try:
        results, failed, retry = emdbid_info(emdb_id)
        fitted_structures = results[0]["fitted_structures"]
    except (ValueError, IndexError) as e:
        print(e)
        print("Trying to get fitted structures from fallback API.")
        results, failed, retry = emdbid_info_newApi(emdb_id)
        fitted_structures = results[0]["fitted_structures"]
        if not isinstance(fitted_structures, list):
            fitted_structures = [fitted_structures]
    for structure in fitted_structures:
        download_pdb(pdb_id=structure, dest_dir=structure_dir, filetype="cif")
    if not len(fitted_structures):
        print(f"Found no structures associated with {emdb_id}.")

    download_ok = validate_download(
        join(target_path, "map", f"{emdb_id.lower().replace('-', '_')}.map.gz"),
        *[
            join(target_path, "fittedModels", "mmCIF", f"{struct}.cif")
            for struct in fitted_structures
        ],
    )

    return target_path, download_ok


@async_run
async def download_af_models(
    uniprot_ids: [str],
    dest_dir: str = abspath("."),
    filetype: str = "pdb",
    af_version: str = "v4",
) -> None:
    base_url = "https://alphafold.ebi.ac.uk/files"
    urls = [
        f"{base_url}/AF-{uniprot_id}-F1-model_{af_version}.{filetype}"
        for uniprot_id in uniprot_ids
    ]
    target_paths = [os.path.join(dest_dir, basename(url)) for url in urls]

    async with httpx.AsyncClient() as client:
        for url, target in zip(urls, target_paths):
            print(url, target)
            with open(target, "wb") as ofile:
                async with client.stream("GET", url) as response:
                    async for chunk in response.aiter_bytes():
                        ofile.write(chunk)

    download_ok = all([validate_download(target) for target in target_paths])

    return target_paths, download_ok


def _extract_gzip(path: str) -> str:
    """Extracts the contents of a gzip file."""
    new_path = path.replace(".gz", "")
    with gzip.open(path, "rb") as f_in:
        with open(new_path, "wb") as f_out:
            copyfileobj(f_in, f_out)
    os.remove(path)
    return new_path
