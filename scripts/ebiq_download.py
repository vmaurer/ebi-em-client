#!python3
import argparse
import hashlib

from re import search
from os import walk, listdir, getcwd
from os.path import exists, isdir, join, islink

import yaml
from ebiq.metadata_queries import emdbid_info, pdbid_info, emdbid_info_newApi
from ebiq.download_files import download_map, download_pdb, download_bundle, Time


def parse_args():
    parser = argparse.ArgumentParser(
        description="Download structures and em-maps from ebi's emdb."
        " Structures are downloaded as cif, em-maps as gzip compressed mrc."
    )
    parser.add_argument(
        "-i",
        dest="input",
        type=str,
        required=True,
        help="PDBID ([a-z0-9]{4}) or EMDBID (EMD-[0-9]{4}).",
    ),
    parser.add_argument(
        "-m",
        help="If provided an EMDBID, do not download the corresponding structures.",
        action="store_true",
        default=False,
    ),
    parser.add_argument(
        "-c",
        dest="write_checksum",
        help="Write MD5 of downloaded files to '{path_to_file}.md5'.",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-o",
        dest="outdir",
        type=str,
        required=False,
        help="Path to output directory.",
        default=getcwd(),
    )
    args = parser.parse_args()
    pdbid_check = search(r"[a-z0-9]{4}", args.input)
    emdbid_check = search(r"EMD-[0-9]\d+", args.input)

    if pdbid_check:
        pdbid_check = pdbid_check.group(0) == args.input
    if emdbid_check:
        emdbid_check = emdbid_check.group(0) == args.input

    if pdbid_check and not emdbid_check:
        args.func = download_pdb
    elif not pdbid_check and emdbid_check:
        args.func = download_bundle_info
        if args.m:
            args.func = download_map
    else:
        raise ValueError(f"{args.input} appears to be not a PDB or EMDB ID.")
    return args


def download_bundle_info(emdb_id: str, dest_dir: str):
    target_path = download_bundle(emdb_id=emdb_id, dest_dir=dest_dir)
    info_dir = join(dest_dir, emdb_id)
    results, failed, retry = emdbid_info(emdbids=emdb_id)
    if len(failed):
        print("Trying to get map metadata from fallback api.")
        results, failed, retry = emdbid_info_newApi(emdbids=emdb_id)
    info = results[0]
    with open(join(info_dir, f"{emdb_id}_info.yaml"), "w", encoding="utf-8") as ofile:
        yaml.dump(info, ofile, default_flow_style=False)
    pdb_files = listdir(join(info_dir, "fittedModels/mmCIF"))
    pdb_files = [
        file.replace(".cif", "") for file in pdb_files if file.endswith(".cif")
    ]
    for pdb_id in pdb_files:
        pdb_info = pdbid_info(pdbids=pdb_id)[0]
        with open(
            join(info_dir, f"{pdb_id}_info.yaml"), "w", encoding="utf-8"
        ) as ofile:
            yaml.dump(pdb_info, ofile, default_flow_style=False)
    return target_path


def calculate_file_md5(fname: str) -> str:
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as file:
        for chunk in iter(lambda: file.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def md5(filepath: str) -> dict:
    md5_dict = {}
    if not exists(filepath):
        raise FileNotFoundError(f"{filepath} not found.")
    file_list = [
        filepath,
    ]
    if isdir(filepath):
        for path, _, files in walk(filepath, followlinks=False):
            file_list = [
                join(path, file)
                for file in files
                if not islink(join(path, file)) and not file.endswith(".md5")
            ]
    md5_dict = {file: calculate_file_md5(file) for file in file_list}

    if isdir(filepath):
        md5_string = b""
        hash_md5 = hashlib.md5()
        for k in md5_dict:
            md5_string += md5_dict[k].encode()
        hash_md5.update(md5_string)
        md5_dict[filepath] = hash_md5.hexdigest()
    return md5_dict


if __name__ == "__main__":
    args = parse_args()
    timer = Time()
    target_path = args.func(args.input, dest_dir=args.outdir)
    md5_dict = md5(target_path)

    timer.elapsed_display()
    print("Filepath\tMD5")
    for key, value in md5_dict.items():
        if args.write_checksum:
            with open(f"{key}.md5", "w", encoding="utf-8") as md5_out:
                md5_out.write(value)
        print(f"{key}\t{value}")
