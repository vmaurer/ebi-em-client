from distutils.core import setup

if __name__ == "__main__":
    setup(
        name="ebiq",
        author="Valentin Maurer",
        author_email="valentin.maurer@embl-hamburg.de",
        version="1.0",
        description="Accessories for interacting with the EBI EMDB",
        packages=["ebiq"],
        scripts=["scripts/ebiq_download.py"],
        install_requires=[
            "ftputil",
            "PyYAML",
            "wget",
            "httpx",
            "beautifulsoup4"
        ],
    )
